class ApplicationController < ActionController::Base
  # デバイスでユーザーネームを保存
  before_action :configure_permitted_parameters, if: :devise_controller?

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  # ヘルパーの読み込み
  include ApplicationHelper

  private
  #  ユーザーネームで登録
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :user_name
  end

end
