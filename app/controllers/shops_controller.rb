class ShopsController < ApplicationController
  # ログイン必須
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  # ビフォーアクション
  before_action :set_shop, only: [:show, :edit, :update, :destroy, :correct_user, :liking_users]
  # ログイン中のユーザーのビフォーアクション
  # before_action :correct_user, only:[:edit, :update]
  # 管理者オンリー
  before_action :authorize_admin, only:[:new, :create, :edit, :update, :destroy]
  # 店舗一覧
  def index
    @q = Shop.ransack(params[:q])
    @shops = @q.result(distinct: true).order(created_at: :desc).page(params[:page]).per(12)
  end
  # 店舗詳細
  def show
    @hash = Gmaps4rails.build_markers(@shop) do |shop, marker|
      marker.lat shop.latitude
      marker.lng shop.longitude
    end
    @comment = Comment.new
    @comments = @shop.comments.order(created_at: :desc).page(params[:page]).per(10)
  end
  # 店舗投稿
  def new
    @shop = Shop.new
  end
  # 店舗投稿
  def create
    @shop = current_user.shops.build(shop_params)
    if @shop.save
      flash[:success] = "投稿しました！"
      redirect_to @shop
    else
      render :new
    end
  end
  # 店舗編集
  def edit
    #code
  end
  # 店舗編集
  def update
    if @shop.update(shop_params)
      flash[:success] = "編集しました。"
      redirect_to @shop
    else
      render :edit
    end
  end
  # 店舗削除
  def destroy
    @shop.destroy
    flash[:success] = "投稿を削除しました。"
    redirect_to root_path
  end
  # 店舗にいいね!しているユーザー
  def liking_users
    @users = @shop.liking_users.order(created_at: :desc).page(params[:page]).per(10)
    @title = "いいね！したユーザー"
  end
  # 検索
  def search
    index
    render :index
  end


  private
  # セット
  def set_shop
    @shop = Shop.find(params[:id])
  end
  # ストロングパラメーター
  def shop_params
    params.require(:shop).permit(:shop_name, :shop_bio, :tel, :location, :latitude, :longitude, :hours, :closed, :shop_image, :shop_image_cache, :url, :user_id, :area_id, :tag_list)
  end
  # 正しいユーザーか
  # def correct_user
    # unless current_user?(@shop.user)
      # redirect_to root_path
    # end
  # end
  # 管理者かどうか
  def authorize_admin
    unless current_user.admin?
      redirect_to root_path
    end
  end

end
