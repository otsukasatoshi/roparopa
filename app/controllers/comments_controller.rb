class CommentsController < ApplicationController
  # ログイン必須
  before_action :authenticate_user!
  # ログイン中のユーザーのビフォーアクション
  before_action :correct_user, only:[:edit, :update]

  # コメント投稿
  def create
    @shop = Shop.find(params[:shop_id])
    @comment = current_user.comments.build(comment_params)
    @comment.shop_id = @shop.id
    if @comment.save
      flash[:success] = "コメントを投稿しました。"
      redirect_to @shop
    else
      flash[:danger] = "コメントを入力してください。"
      @comments = @shop.comments.order(created_at: :desc).page(params[:page]).per(10)
      render "shops/show"
    end
  end
  # コメント削除
  def destroy
    @shop = Shop.find(params[:shop_id])
    @comment = current_user.comments.find_by(shop_id: @shop.id)
    @comment.destroy
    flash[:success] = "コメントを削除しました。"
    redirect_to @shop
  end

  private
  # ストロングパラメーター
  def comment_params
    params.require(:comment).permit(:content, :user_id, :shop_id)
  end
  # 正しいユーザーか
  def correct_user
    @comment = Comment.find(params[:id])
    unless current_user?(@comment.user)
      redirect_to root_path
    end
  end

end
