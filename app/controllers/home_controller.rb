class HomeController < ApplicationController
  #トップページ
  def top
    @q = Shop.ransack(params[:q])
    if user_signed_in?
      @shops = Shop.all.order(created_at: :desc).page(params[:page]).per(12)
    else
      @shops = Shop.all.order(created_at: :desc).limit(6)
    end
  end

  # 掲載説明
  def published
    #code
  end

end
