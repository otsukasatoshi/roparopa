class UsersController < ApplicationController
  # ログイン必須
  before_action :authenticate_user!
  # ビフォーアクション
  before_action :set_user, only: [:show, :edit, :update, :destroy, :correct_user]
  # ログイン中のユーザーのビフォーアクション
  before_action :correct_user, only:[:edit, :update]

  # ユーザー詳細
  def show
    @shops = @user.like_shops.order(created_at: :desc)
    @comments = @user.comments.order(created_at: :desc)
    @comment_shops = @user.comment_shops.order(created_at: :desc)
  end
  # ユーザー編集
  def edit
    #codes
  end
  # ユーザー編集
  def update
    if @user.update(user_params)
      flash[:success] = "ユーザーが更新されました。"
      redirect_to @user
    else
      render :edit
    end
  end
  # ユーザー削除
  def destroy
    @user.destroy
    flash[:success] = "ユーザーを削除しました。"
    redirect_to root_path
  end

  private
  # ビフォーアクション
  def set_user
    @user = User.find(params[:id])
  end
  # ストロングパラメーター
  def user_params
    params.require(:user).permit(:user_name, :email, :user_image, :user_image_cache)
  end
  # 正しいユーザーか
  def correct_user
    unless current_user?(@user)
      redirect_to root_path
    end
  end

end
