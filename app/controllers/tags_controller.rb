class TagsController < ApplicationController
  # タグ一覧
  def index
    @tags = ActsAsTaggableOn::Tag.all
  end
  # タグに紐付いた店舗一覧
  def show
    @tag =  ActsAsTaggableOn::Tag.find(params[:id])
    @shops = Shop.tagged_with(@tag.name).page(params[:page]).per(12)
  end
end
