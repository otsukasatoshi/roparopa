class LikesController < ApplicationController
  before_action :authenticate_user!
  # いいねアクション
  def like
    @shop = Shop.find(params[:shop_id])
    like = current_user.likes.build(shop_id: @shop.id)
    like.save
  end
  # いいね取り消しアクション
  def unlike
    @shop = Shop.find(params[:shop_id])
    like = current_user.likes.find_by(shop_id: @shop.id)
    like.destroy
  end
end
