class Comment < ActiveRecord::Base
  # アソシエーション
  belongs_to :user
  belongs_to :shop
  # バリデーション
  validates :content, presence: true
end
