class Contact

  include ActiveModel::Model

  attr_accessor :email, :message

  validates :email, :presence => {:message => 'メールを入力してください'}
  validates :message, :presence => {:message => 'メッセージを入力してください'}

end
