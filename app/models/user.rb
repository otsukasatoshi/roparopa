class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable #:trackable, :validatable
  # ユーザー画像アップロード
  mount_uploader :user_image, UserImageUploader
  # バリデーション
  validates :user_name, presence: true
  # アソシエーション
  has_many :shops, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :like_shops, through: :likes, source: :shop
  has_many :comments, dependent: :destroy
  has_many :comment_shops, through: :comments, source: :shop

end
