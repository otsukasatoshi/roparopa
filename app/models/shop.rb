require 'file_size_validator'
class Shop < ActiveRecord::Base
  # 店舗画像アップロード
  mount_uploader :shop_image, ShopImageUploader
  # バリデーション
  validates :shop_name, presence: true
  validates :shop_bio, length: {maximum: 300}
  validates :location, presence: true
  validates :user_id, presence: true
  # 投稿の際の画像のサイズをバリデーション
  validates :shop_image, file_size: {maximum: 5.megabytes.to_i} # 最大5MBに制限
  # アソシエーション
  belongs_to :user
  has_many :likes, dependent: :destroy
  has_many :liking_users, through: :likes, source: :user
  has_many :comments, dependent: :destroy
  belongs_to :area
  # google map
  geocoded_by :location
  after_validation :geocode, :if => :location_changed?
  # タグ
  acts_as_taggable_on :tags
end
