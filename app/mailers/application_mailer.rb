class ApplicationMailer < ActionMailer::Base
  default from: "info@ropa-ropa.com"
  layout 'mailer'
end
