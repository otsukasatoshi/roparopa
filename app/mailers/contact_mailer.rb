class ContactMailer < ApplicationMailer
  # お問い合わせメール
  def new_contact(contact)
    @contact = contact
    mail to: "info@ropa-ropa.com", subject: "[ロパロパ]新しいお問い合わせ"
  end

end
