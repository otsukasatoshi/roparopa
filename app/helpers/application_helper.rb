module ApplicationHelper
  # ページタイトル
  def page_title
    title = "古着屋情報サイトのRopaRopa(ロパロパ)"
    title = @page_title + " | " + title if @page_title
    title
  end
  # ログインしてれば編集できる
  def current_user?(user)
    current_user == user
  end
end
