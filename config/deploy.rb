# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'roparopa'
set :repo_url, 'git@bitbucket.org:OtsukaSatoshi/roparopa.git'

# Default branch is :master
set :branch, 'master'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/roparopa/'

# Default value for :scm is :git
set :scm, :git
set :deploy_via, :remote_cache
# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system', 'public/user_images')

# Default value for default_env is {}
set :default_env, {
	:S3_ACCESS_KEY => ENV['S3_ACCESS_KEY'],
	:S3_SECRET_KEY => ENV['S3_SECRET_KEY']
}

set :rbenv_type, :user
set :rbenv_ruby, '2.2.2'
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value


# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

	before 'deploy:publishing', 'db:seed_fu'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
