Rails.application.routes.draw do
  # デバイス
  devise_for :users
  # 管理画面
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # ルート
  root "home#top"
  # ユーザーのルーティング
  resources :users, only:[:show, :edit, :update, :destroy]

  # 店舗のルーティング
  resources :shops do
    resources :comments, only:[:create, :destroy]
    member do
      get :liking_users
    end
    collection do
      match 'search' => 'shops#search', via: [:get, :post], as: :search
    end
  end
  # タグ機能
  resources :tags, only: [:index, :show]
  # いいねする
  post "/like/:shop_id" => "likes#like", as: "like"
  # いいね取り消し
  delete "/unlike/:shop_id" => "likes#unlike", as: "unlike"
  #お問い合わせ
  get "/contact" => "contacts#new", as: "contact"
  post "/contact" => "contacts#create"
  # 掲載ページのルーティング
  get "/published" => "home#published"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
