class RenameTellColumnToShops < ActiveRecord::Migration
  def change
    rename_column :shops, :tell, :tel
  end
end
