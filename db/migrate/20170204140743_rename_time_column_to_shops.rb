class RenameTimeColumnToShops < ActiveRecord::Migration
  def change
    rename_column :shops, :time, :hours
  end
end
