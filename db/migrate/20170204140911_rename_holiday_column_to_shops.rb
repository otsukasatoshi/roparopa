class RenameHolidayColumnToShops < ActiveRecord::Migration
  def change
    rename_column :shops, :holiday, :closed
  end
end
