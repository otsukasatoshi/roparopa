class CreateShops < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :shop_name
      t.text :shop_bio
      t.string :tell
      t.string :location
      t.string :time
      t.string :holiday
      t.string :shop_image
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :shops, [:user_id,:created_at]
  end
end
