class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.integer :user_id
      t.integer :shop_id

      t.timestamps null: false
    end
    add_index :likes, :shop_id
    add_index :likes, :user_id
  end
end
