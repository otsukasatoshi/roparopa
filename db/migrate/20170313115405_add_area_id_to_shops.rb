class AddAreaIdToShops < ActiveRecord::Migration
  def change
    add_column :shops, :area_id, :integer
    add_index :shops, :area_id
  end
end
