Area.seed do |s|
  s.id    = 1
  s.name  = "原宿"
end

Area.seed do |s|
  s.id    = 2
  s.name  = "下北沢"
end

Area.seed do |s|
  s.id    = 3
  s.name  = "高円寺"
end

Area.seed do |s|
  s.id    = 4
  s.name  = "渋谷"
end

Area.seed do |s|
  s.id    = 5
  s.name  = "代官山・中目黒"
end

Area.seed do |s|
  s.id    = 6
  s.name  = "梅田・中崎町"
end

Area.seed do |s|
  s.id    = 7
  s.name  = "心斎橋・アメリカ村"
end

Area.seed do |s|
  s.id    = 8
  s.name  = "堀江"
end

Area.seed do |s|
  s.id    = 9
  s.name  = "南船場"
end

Area.seed do |s|
  s.id    = 10
  s.name  = "栄・大須"
end
